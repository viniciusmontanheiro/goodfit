'use strict';

/**
 * @description Implementação do index controller
 */

var User = require("../models/User");

var UserController = {};


UserController.create = function(req,res,next){

    var m = new Messages();

    User.findOne({'username': req.body.username})
        .populate('user', '-password')
        .exec(function (err, user) {

            var errMessage = "";

            if (err) {
                console.error(err);
                return Transporter.json(res, m.ACCOUNTERROR);
            }

            // Verifica se já existe usuário com esse email
            if (user) {
                console.log(err);
                return Transporter.json(res, m.USEREXISTS);
            } else {

                if(Util.isEmpty(req.body,['name','username','password'])){
                    return Transporter.json(res, m.ACCOUNTERROR);
                }

                //Se não, o criamos
                var newUser = new User();
                newUser.name = req.body.name;
                newUser.username = req.body.username;
                newUser.email = req.body.email;
                newUser.phone = req.body.phone;
                newUser.password = newUser.generateHash(req.body.password);
                newUser.active = false;
                newUser.lastUpdate = Date.now();

                // Salva o usuário
                newUser.save(function (err,user) {
                    if (err) {
                        console.error(err);
                    }
                    m.GETTED.DATA = user;
                    return Transporter.json(res, m.GETTED);
                });
            }
        });
};

//ListAll
//========================================
UserController.listAll = function(req, res, next){
    var m = new Messages();

    User.find(function (err, users) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.HANDLEERROR);
        }

        m.GETTED.DATA = users;
        return Transporter.json(res, m.GETTED);
    });
};

//Update
//========================================
UserController.update = function(req, res, next){
    var m = new Messages();
    var user = req.body;

    if (Util.isEmpty(user,['_id'])) {
        return Transporter.emit(res, m.CHANGEERROR);
    }else{
        if(Util.isEmpty(user,'password')){
            delete user.password;
        }else{
            user.password = User.generateHash(user.password);
        }
    }

    var id = user._id;
    delete user._id;

    User.update({_id: id}, user, {upsert: true}, function (err) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.CHANGEERROR);
        }
        return Transporter.json(res, m.CHANGED);
    });
};

//Delete
//========================================
UserController.delete = function(req, res, next){
    var m = new Messages();
    var id = req.params.id;

    User.findOne({_id: id},function (err,retorno) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.REMOVEERROR);
        }

        retorno.remove(function(err){
            if(err){
                return Transporter.json(res, m.REMOVEERROR);
            }
            return Transporter.json(res, m.REMOVED);
        });

    });
};


//Login
//========================================
UserController.login = function login(passport){
    return passport.authenticate('admin-login', {
        successRedirect : '/dashboard',
        failureRedirect : '/entrar',
        failureFlash : true
    });
};


//Cadastrar
//========================================
UserController.signIn = function (passport){
    return passport.authenticate('new-account', {
        successRedirect: '/dashboard',
        failureRedirect: '/entrar'
    });
};

//Logout
//========================================
UserController.logout = function(req,res,next){
    req.logout();
    return res.redirect('/entrar');
};

module.exports = UserController;



