
'use strict';

/**
 * @desc Controle responsável pelo tratamento das imagens
 * @since 02/12/2014
 */

var mongoose = require('mongoose')
    , fs = require('fs')
    , Images = require('../models/Images')
    , Produto = require('../models/Produto');


// NEW UPLOAD
exports.upload = function (req, res) {

    var oldPath = req.files.myFile.path;
    var separator = '/';
    var filename = oldPath.split(separator)[oldPath.split(separator).length - 1];
    var newPath = __dirname;
    newPath = newPath.split("/server/");
    newPath = [newPath[0],'client', 'assets', 'images', 'uploads', filename].join(separator);
    //MUDAR O CAMINHO PARA S.O WINDOWS

    console.log('__dirname', __dirname);
    console.log('oldPath', oldPath);
    console.log('newPath: ', newPath);
    console.log('filename: ', filename);

    fs.rename(oldPath, newPath, function (err) {
        if (err === null) {
            var image = {
                principal: req.body.principal || false,
                image: {
                    modificationDate: req.files.myFile.modifiedDate || new Date(),
                    name: req.files.myFile.name || "???",
                    size: req.files.myFile.size || 0,
                    type: req.files.myFile.type || "???",
                    filename: filename
                }
            };

            console.log('Renaming file to ', req.files.myFile.name);

            var promise = {};
            
            if(!Util.isEmpty(req.body,['productId'])){
                var productId = req.body.productId;

                promise = Images.create(image);

                promise.then(function(newImage){
                    var m = new Messages();

                    if(newImage != null && newImage != undefined){

                        Produto.update({_id: productId},  {$push: {photos: newImage}}, {upsert: true},
                            function (err,produto) {
                                if (err) {
                                    console.error(err);
                                    return Transporter.json(res, m.CHANGEERROR);
                                }
                                
                                console.log(produto);

                                var retObj = {
                                    meta: {"action": "upload", 'timestamp': new Date(), filename: __filename},
                                    doc: produto,
                                    err: err
                                };
                                return res.send(retObj)
                            });
                    }else{
                        console.error('Produto sem imagens!');
                        var retObj = {
                            meta: {"action": "upload", 'timestamp': new Date(), filename: __filename},
                            doc: newImage,
                            err: err
                        };
                        return res.send(retObj)
                    }

                });
            }else{
                console.error('Imagem sem o ID do produto!');
                var retObj = {
                    meta: {"action": "upload", 'timestamp': new Date(), filename: __filename},
                    doc: req.body,
                    err: err
                };
                return res.send(retObj)
            }


        }else{
            console.error('Falha no upload!');
            var retObj = {
                meta: {"action": "upload", 'timestamp': new Date(), filename: __filename},
                doc: req.body,
                err: err
            };
            return res.send(retObj)
        }
    });
};

//
//// GET BY IMAGE AND PRODUCT ID
exports.list = function (req, res) {
    var produtoId = req.params.id;

    var m = new Messages();

    Produto.findOne({'_id': produtoId})
        .populate('photos')
        .exec(function (err, produto) {
            m.GETTED.DATA = produto.photos;
            return Transporter.json(res, m.GETTED);
        });
};

//
//// GET BY IMAGE AND PRODUCT ID
exports.listSlides = function (req, res) {
    var m = new Messages();

    Images.find({'slideshow': true})
        .exec(function (err, images) {
            m.GETTED.DATA = images;
            return Transporter.json(res, m.GETTED);
        });
};

exports.updateStatus = function (req, res) {

    var m = new Messages();

    if (Util.isEmpty(req.body, ['_id'])) {
        return Transporter.json(res, m.CHANGEERROR);
    }

    var params = {
        _id : req.body._id,
        principal : req.body.principal
    };

    var imageStatus = new Images(params);
    imageStatus.statusUpdate();
    Transporter.json(res, m.CHANGED);
};

exports.updateSlideShow = function (req, res) {

    var m = new Messages();

    if (Util.isEmpty(req.body, ['_id'])) {
        return Transporter.json(res, m.CHANGEERROR);
    }

    var params = {
        _id : req.body._id,
        slideshow : req.body.slideshow
    };

    var imageStatus = new Images(params);
    imageStatus.setSlideShow();
    Transporter.json(res, m.CHANGED);
};

exports.setTnutricional = function (req, res) {

    var m = new Messages();

    if (Util.isEmpty(req.body, ['_id'])) {
        return Transporter.json(res, m.CHANGEERROR);
    }

    var params = {
        _id : req.body._id,
        tnutricional : req.body.tnutricional
    };

    var imageStatus = new Images(params);
    imageStatus.setTnutricional();
    Transporter.json(res, m.CHANGED);
};

exports.ativarImagem = function (req, res) {

    var m = new Messages();

    if (Util.isEmpty(req.body, ['_id'])) {
        return Transporter.json(res, m.CHANGEERROR);
    }

    var params = {
        _id : req.body._id,
        ativo : req.body.ativo
    };

    var imageStatus = new Images(params);
    imageStatus.ativarImagem();
    Transporter.json(res, m.CHANGED);
};


//// DELETE
exports.delete = function (req, res) {
    var id = req.params.id;

    var m = new Messages();
    console.log(id);

    if (Util.isEmpty(req.params,['id'])) {
        return Transporter.json(res, m.REMOVEERROR);
    }

    Images.findOne({_id: id},function (err,retorno) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.REMOVEERROR);
        }

        retorno.remove(function(err){
            if(err){
                return Transporter.json(res, m.REMOVEERROR);
            }

            Produto.update({photos : id}, { $pull: { 'photos': id } }, {upsert: true},
                function (err) {
                    if(err){
                        return Transporter.json(res, m.REMOVEERROR);
                    }
                    return Transporter.json(res, m.REMOVED);
                });
        });

    });
};

