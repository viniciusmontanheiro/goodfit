'use strict';

/**
 * @description Implementação
 */

var Cliente = require("../models/Cliente");
var ClienteController = {};

//ListAction - Lista todos os produtos cadastrados
//========================================
ClienteController.list = function (req, res, next) {
    var m = new Messages();

    Cliente.find({})
        .exec(function (err, produtos) {
            m.GETTED.DATA = produtos;
            return Transporter.json(res, m.GETTED);
        });
};

//CreateAction
//========================================
ClienteController.create = function (req, res, next) {
    var m = new Messages();
    var cliente = new Cliente(req.body);

    cliente.save(function (err) {
        if (err) {
            return Transporter.json(res, m.CREATEERROR);
        }
        return Transporter.json(res, m.CREATED);
    });

};


//UpdateAction
//========================================
ClienteController.update = function (req, res, next) {
    var m = new Messages();
    var cliente = req.body;

    if (Util.isEmpty(cliente, ['_id'])) {
        return Transporter.json(res, m.CHANGEERROR);
    }

    var id = cliente._id;
    delete cliente._id;

    Cliente.update({_id: id}, cliente, {upsert: true}, function (err) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.CHANGEERROR);
        }
        return Transporter.json(res, m.CHANGED);
    });

};

//RemoveAction
//========================================
ClienteController.delete = function (req, res, next) {
    var m = new Messages();
    var id = req.params.id;

    Cliente.findOne({_id: id},function (err,retorno) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.REMOVEERROR);
        }

        retorno.remove(function(err){
            if(err){
                return Transporter.json(res, m.REMOVEERROR);
            }
            return Transporter.json(res, m.REMOVED);
        });

    });
};


module.exports = ClienteController;




