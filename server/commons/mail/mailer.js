/**
 * Created by vinicius on 06/04/15.
 * Exemplo : No seu controle
 *   var config = {
                name : 'GMAIL',
                sender : 'contato.expirou@gmail.com',
                auth : 'Security',
                headers : {
                    from: 'contato.expirou@gmail.com',
                    to: 'contato.expirou@gmail.com',
                    subject: 'Novo Contato | Expirou App - ' + dest.name,
                    html: '<p>' +dest.message+'</p><p>E-mail:'+ dest.email +'</p>'
                },
                success : function(){},
                error: function(){}
            };
 mail.send(config);
 */

var nodemailer = require('nodemailer');

function Mail() {

    this.send = function (config) {
        var transporter = nodemailer.createTransport({
            service: config.name,
            auth: {
                user: config.sender,
                pass: config.auth
            }
        });

        var mailOptions = {
            from: config.headers.from,
            to: config.headers.to,
            subject: config.headers.subject,
            html: config.headers.html
        };

        transporter.sendMail(mailOptions, function (error, info) {

            if (error) {
                console.error(error);
                return config.error();
            } else {
                console.info('Message sent: ' + info.response);
                return config.success();
            }
        });
    }

};

module.exports = Mail;

