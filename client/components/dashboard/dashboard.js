'use strict';

/**
 * Created by viniciusmontanheiro on 23/06/15.
 */

angular.module('app.dashboard', [])
    .controller('DashboardController', ['ApiService', 'MessageService','Spinner',
        function (ApiService,MessageService,Spinner) {

        var self = this;
        self.teste = 'kkkk';
        self.products = [];
        self.sales = [];
        self.users = [];
        self.images = [];
        self.categories = [];
        self.showProducts = false;
        self.showCategories = false;
        self.showImages = false;
        self.showUsers = false;
        self.showSales = false;
        self.init = init;
        self.init();
        self.loaded = false;

        function init(){
            Spinner.stop();
            Spinner.start();


            ApiService.query('/history/products').success(function(retorno) {
                Spinner.stop();
                self.products = retorno.DATA;

                if(self.products && self.products.length >=1){
                    self.showProducts = true;
                }else{
                    self.showProducts = false;
                }

            }).error(function(data, status) {
                Spinner.stop();
                self.showProducts = false;
                MessageService.addError("Erro ao listar histórico de produtos!")
            });

            ApiService.query('/history/sales').success(function(retorno) {
                Spinner.stop();
                self.sales = retorno.DATA;

                if(self.sales && self.sales.length >=1){
                    self.showSales= true;
                }else{
                    self.showSales = false;
                }

            }).error(function(data, status) {
                Spinner.stop();
                self.showSales = false;
                MessageService.addError("Erro ao listar histórico de vendas!")
            });

            ApiService.query('/history/categories').success(function(retorno) {
                Spinner.stop();
                self.categories = retorno.DATA;

                if(self.categories && self.categories.length >=1){
                    self.showCategories = true;
                }else{
                    self.showCategories = false;
                }

            }).error(function(data, status) {
                Spinner.stop();
                self.showCategories = false;
                MessageService.addError("Erro ao listar histórico de categorias!")
            });

            ApiService.query('/history/clients').success(function(retorno) {
                Spinner.stop();
                self.clients = retorno.DATA;

                if(self.clients && self.clients.length >=1){
                    self.showClients= true;
                }else{
                    self.showClients = false;
                }

            }).error(function(data, status) {
                Spinner.stop();
                self.showClients = false;
                MessageService.addError("Erro ao listar histórico de clientes!")
            });

            ApiService.query('/history/users').success(function(retorno) {
                Spinner.stop();
                self.users = retorno.DATA;

                if(self.users && self.users.length >=1){
                    self.showUsers = true;
                }else{
                    self.showUsers = false;
                }

            }).error(function(data, status) {
                Spinner.stop();
                self.showUsers = false;
                MessageService.addError("Erro ao listar histórico de usuários!")
            });

            ApiService.query('/history/images').success(function(retorno) {
                Spinner.stop();
                self.images = retorno.DATA;

                if(self.images && self.images.length >=1){
                    self.showImages = true;
                }else{
                    self.showImages = false;
                }

            }).error(function(data, status) {
                Spinner.stop();
                self.showImages = false;
                MessageService.addError("Erro ao listar histórico de imagens!")
            });
        }

    }]);