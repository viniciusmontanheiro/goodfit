'use strict';

var path = require('path');
var server = path.normalize(__dirname);

/**
 * @description Módulo de configurações do grunt
 * @author Vinícius Montanheiro
 * @param grunt cli
 **/
function Grunt(grunt) {
    grunt.initConfig({

        shell: {
            options: {
                stderr: false
            },
            mongo: {
                command: 'sudo mongod'
            },
            forever: {
                command: 'forever start app.js'
<<<<<<< HEAD
            },
            forever_win: {
                command: 'node app.js'
=======
>>>>>>> 878ede3bd8722613ed59d5c4b167c65e09f17308
            }
        },

        imagemin: {
            png: {
                options: {
                    optimizationLevel: 7
                },
                files: [
                    {
                        expand: true,
                        cwd: '../client/assets/images/uploads/',
                        src: ['*.png'],
                        dest:'../client/assets/images/uploads/compressed/',
                        ext: 'png'
                    }
                ]
            },
            jpg: {
                options: {
                    progressive: true
                },
                files: [
                    {
                        expand: true,
                        cwd: '../client/assets/images/uploads/',
                        src: ['*.jpg'],
                        dest:'../client/assets/images/uploads/compressed/',
                        ext: 'jpg'
                    }
                ]
            }
        }, // imagemin

        concat: {
            options: {
                separator: ';'
            },
            bowerjs: {
                files: [
                    {
                        dest: '../client/assets/components/main.js',
                        src: ['../client/assets/components/angular-bootstrap/ui-bootstrap.js'
                            , '../client/assets/components/angular-ui-router/release/angular-ui-router.js'
                            , '../client/assets/components/angular-file-upload/angular-file-upload.js'
                            , '../client/assets/components/angular-resource/angular-resource.js'
                            , '../client/assets/components/angular-sanitize/angular-sanitize.js'
                            , '../client/assets/components/angularjs-toaster/toaster.js'
                            , '../client/assets/components/jquery/dist/jquery.js'
                            , '../client/assets/components/bootstrap/dist/js/bootstrap.js'
                            , '../client/assets/components/jquery.countdown/dist/jquery.countdown.js'
                            , '../client/assets/components/ngDialog/js/ngDialog.js']
                    }
                ]
            },
            bowercss: {
                files: [
                    {
                        dest: '../client/assets/components/main.css',
                        src: ['../client/assets/components/angularjs-toaster/toaster.css'
                            , '../client/assets/components/bootstrap/dist/css/bootstrap.css'
                            , '../client/assets/components/bootstrap/dist/css/bootstrap-theme.css'
                            , '../client/assets/components/font-awesome/css/font-awesome.css'
                            , '../client/assets/components/ngDialog/css/ngDialog.css'
                            , '../client/assets/components/ngDialog/css/ngDialog-theme-default.css']
                    }
                ]
            },
            amdcss: {
                files: [
                    {
                        dest: '../client/assets/css/main.css',
                        src: '../client/assets/css/*.css'
                    }
                ]
            },
            amdjs: {
                files: [
                    {
                        dest: '../client/assets/js/main.js',
                        src: '../client/assets/js/*.js'
                    }
                ]
            }
        },
        uglify: {
            bowerjs: {
                files: [
                    {
                        dest: '../client/assets/components/main.min.js',
                        src: '../client/assets/components/main.js'
                    }
                ]
            },
            controllers: {
                files: [{
                    expand: true,
                    cwd: '../client/scripts/controllers/',
                    src: '*.js',
                    dest: '../client/scripts/controllers/compressed',
                    ext: 'min.js'
                }]
            },
            directives: {
                files: [{
                    expand: true,
                    cwd: '../client/scripts/directives/',
                    src: '*.js',
                    dest: '../client/scripts/directives/compressed',
                    ext: 'min.js'
                }]
            },
            services: {
                files: [{
                    expand: true,
                    cwd: '../client/scripts/services/',
                    src: '*.js',
                    dest: '../client/scripts/services/compressed',
                    ext: 'min.js'
                }]
            },
            server: {
                files: [{
                    expand: true,
                    cwd: server,
                    src: '**/*.js',
                    dest: server,
                    ext: '.min.js'
                }]
            }
        },

        cssmin: {
            amdcss: {
                files: [{
                    expand: true,
                    cwd: '../client/assets/css/',
                    src: ['*.css'],
                    dest: '../client/assets/css/compressed',
                    ext: '.min.css'
                }]
            },
            bowercss: {
                files: [{
                    expand: true,
                    cwd: '../client/assets/components',
                    src: ['*.css'],
                    dest: '../client/assets/components',
                    ext: '.min.css'
                }]
            }
        },

        ngAnnotate: {
            scripts: {
                expand: true,
                src: '../client/scripts/**/*.js'
            }
        }

    });

    /**
     * @description Plugins
     */
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-shell');

    /**
     * @description Minifica arquivos js,css e imagens
     */

    grunt.registerTask('cssmini',['concat:bowercss','cssmin:bowercss']);

    grunt.registerTask('minifica',
        ['concat:bowerjs'
        ,'concat:bowercss'
        ,'concat:amdcss'
        ,'uglify:bowerjs'
        ,'uglify:controllers'
        ,'uglify:directives'
        ,'uglify:services'
        ,'uglify:server'
        ,'cssmin:amdcss'
        ,'cssmin:bowercss'
        ,'imagemin:png'
        ,'imagemin:jpg'
<<<<<<< HEAD
        ,'ngAnnotate']
    );
=======
        ,'ngAnnotate']);
>>>>>>> 878ede3bd8722613ed59d5c4b167c65e09f17308

    /**
     * @description Tarefa para iniciar o mongo/aplicação
     */
    grunt.registerTask('mongo', ['shell:mongo']);
    grunt.registerTask('start', ['shell:forever']);
<<<<<<< HEAD
    grunt.registerTask('start_win', ['shell:forever_win']);

}
=======
};
>>>>>>> 878ede3bd8722613ed59d5c4b167c65e09f17308

module.exports = Grunt;