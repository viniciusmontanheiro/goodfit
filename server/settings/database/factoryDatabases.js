'use strict';

/**
 * Created by vinicius on 01/04/15.
 */

var Mongo = require('../../settings/database/mongoConnector.js');
var Postgres = require('../../settings/database/postgresConnector.js');

function FactoryDatabases() {

    this.connections = [];
    this.connectionsList = [
        {id: 'mongo', connector: Mongo.connector},
        {id: 'postgres', connector: Postgres.connector}
    ];

    this.getConnections = function () {

        var config = appConfig.databases;

        for (var i = 0; i < config.length; i++) {
            for (var j = 0; j < this.connectionsList.length; j++) {
                if (this.connectionsList[j].id == config[i].id) {
                    this.connections.push(this.connectionsList[j].connector(config[i]));
                }
            }
        }
        return this;
    };
};

module.exports = FactoryDatabases;



