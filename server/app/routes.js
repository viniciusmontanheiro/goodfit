'use strict';

/**
 * @description Unificação das rotas do sistema.
 * @param app
 */

var homeController = require('./controllers/home');
var genericController = require('./controllers/generic');
var imagesController = require('./controllers/images');
var userController = require('./controllers/user');
var produtoController = require('./controllers/produto');
var categoriaController = require('./controllers/categoria');
var vendasController = require('./controllers/vendas');
var historyController = require('./controllers/history');
var clientController = require('./controllers/cliente');

function Routes(passport) {

    //INDEX ROUTES
    //GET
    app.get('/', homeController.home);
    app.get('/entrar', homeController.entrar);
    app.get('/dashboard',isLoggedIn, homeController.dashboard);
    app.post('/contactUs', homeController.enviarContato);

    //POST

    //ADMIN ROUTES
    // Usuarios
    //================================================
    app.post('/login',userController.login(passport));
    app.get('/logout', isLoggedIn, userController.logout);
    app.post('/user/add', userController.create);
    app.get('/user/list', isLoggedIn,userController.listAll);
    app.delete('/user/delete/:id',isLoggedIn, userController.delete);
    app.put('/user/update',isLoggedIn, userController.update);

    // Produtos
    //================================================
    app.get('/product/list', produtoController.list);
    app.post('/product/create',isLoggedIn, produtoController.create);
    app.put('/product/update',isLoggedIn, produtoController.update);
    app.delete('/product/delete/:id',isLoggedIn, produtoController.delete);
    app.put('/product/update/status',isLoggedIn, produtoController.statusUpdate);
    app.put('/product/update/destaque',isLoggedIn, produtoController.statusUpdate);
    app.post('/product/listById', produtoController.listById);

    app.get('/category/list', categoriaController.list);
    app.post('/category/create',isLoggedIn, categoriaController.create);
    app.put('/category/update',isLoggedIn, categoriaController.update);
    app.delete('/category/delete/:id',isLoggedIn, categoriaController.delete);

    //// Imagens
    app.post('/upload', isLoggedIn,imagesController.upload);
    app.get('/image/list/:id', isLoggedIn,imagesController.list);
    app.get('/image/slideshow',imagesController.listSlides);
    app.put('/image/main',isLoggedIn,imagesController.updateStatus);
    app.put('/image/setSlideShow',isLoggedIn,imagesController.updateSlideShow);
    app.put('/image/setTnutricional',isLoggedIn,imagesController.setTnutricional);
    app.put('/image/ativarImagem',isLoggedIn,imagesController.ativarImagem);
    app.delete('/image/delete/:id', isLoggedIn,imagesController.delete);

    //Vendas
    app.get('/sales/list',isLoggedIn, vendasController.list);
    app.delete('/sales/delete/:id',isLoggedIn, vendasController.delete);

    //Histórico
    app.get('/history/products',isLoggedIn,historyController.products);
    app.get('/history/sales',isLoggedIn,historyController.sales);
    app.get('/history/categories',isLoggedIn,historyController.categories);
    app.get('/history/images',isLoggedIn,historyController.images);
    app.get('/history/users',isLoggedIn,historyController.users);
    app.get('/history/clients',isLoggedIn,historyController.clients);

    // Clientes
    //================================================
    app.get('/client/list',isLoggedIn, clientController.list);
    app.post('/client/create', isLoggedIn,clientController.create);
    app.put('/client/update',isLoggedIn, clientController.update);
    app.delete('/client/delete/:id',isLoggedIn, clientController.delete);

    //ISSUE ROUTES
    //================================================
    app.all("*", genericController.error.notFound);
};

// Verifica se o usuário está autenticado
function isLoggedIn(req, res, next) {

    if (req.isAuthenticated()){
        return next();
    }
    res.redirect('/entrar');
};

//Disponibilizando na aplicação
GLOBAL.isLoggedIn = isLoggedIn;

module.exports = Routes;





