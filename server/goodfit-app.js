/**
 * Created by vinicius on 07/04/15.
 */

GLOBAL.appConfig = require('./configs.json');
var Cluster = require('./settings/server/cluster.js');
var Server = require('./settings/server/server.js');

// Variáveis visiveis na aplicação
require('./settings/server/globals.js');

//Instânciando sistema de logs
if(appConfig.actived.logs){
    require('./settings/notifications/logs/logger.js');
}

//Se, inicia aplicação com clusters
if (appConfig.actived.cluster) {
    var clustering = new Cluster();
    clustering.init();
} else {
    Server();
}

