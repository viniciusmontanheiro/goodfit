'use strict';

angular
    .module('app.userForm', [])
    .controller('UserformController', ['ApiService', 'MessageService','Util','$rootScope','$routeParams','$location','Spinner', UserformController]);

function UserformController(ApiService,MessageService,Util,$rootScope,$routeParams,$location,Spinner) {
        
        var self = this;
        
        this.user = {
            name : '',
            username:'',
            email:'',
            phone:'',
            password : ''
        }
        
        this.isUpdate = false;
        
        if($routeParams.action == "edit"){ //Editar
            this.isUpdate = true;
            
            //TODO: load user from API
        }
        
        
        // onClickCadastrar
        //=======================================
        this.onClickCadastrar = function(){

            if(Util.fn.isEmpty(self.user, ['username','password','phone'])){
                return MessageService.addError("Verifique os campos obrigatórios.");
            }

            Spinner.start();
            
            ApiService
                .save("/user/add", self.user)
                .success(function(retorno){
                    Spinner.stop();
                    self.user = {};
                    MessageService.addSuccess("Novo usuário criado com sucesso");
                    $location.path('/users');
                }).error(function(err){
                Spinner.stop();
                    if(err){
                        MessageService.addError(err.MESSAGE);
                    }
                });
        }
        
        
        // onClickUpdate
        //=======================================
        this.onClickUpdate = function(){
            Spinner.start;
            
                ApiService.update('/user/update',self.user).success(function(retorno) {
                    Spinner.stop();
                    MessageService.addSuccess("Usuário alterarado com sucesso!");
                    self.goToList('/listar');
                }).error(function(data, status) {
                    Spinner.stop();
                    MessageService.addError("Erro ao alterar usuário!");
                });
        }

};