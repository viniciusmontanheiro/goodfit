'use strict';

var fs = require("fs");

/**
 * @description Utilidades
 * @constructor
 */
function Util() {
    /**
     * @description Cria um novo arquivo para escrita
     * @param URL
     * @param FLAGS
     * @returns {*}
     */
    this.fnCreateFile = function fnCreateFile(URL, FLAG) {
        return fs.createWriteStream(URL, {
            encoding: 'utf8',
            flags: FLAG
        });
    };

    /**
     * @description Abre arquivo para escrita
     * @param URL
     * @param FLAGS
     * @param CONTENT (conteúdo)
     * @returns {*}
     */
    this.fnWriteFile = function fnWriteFile(URL, FLAGS, CONTENT) {
        return fs.Writer({
            path: URL
            , mode: 755
            , flags: FLAGS
        })
            .write(CONTENT)
            .end()
    };

    /**
     * @desc Valida campos vazios
     * @param objeto,array
     * @returns {boolean}
     */
    this.isEmpty = function isEmpty(obj, arr) {
        if (arr != null && arr != undefined) {
            for (var i = 0; i < arr.length; i++) {
                if (obj.hasOwnProperty(arr[i])) {
                    if ((obj[arr[i]]).toString() == "" || obj[arr[i]] == undefined || obj[arr[i]].length == 0) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        }
        return false;
    };

};

module.exports = Util;





