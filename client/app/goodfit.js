'use strict';

/**
 * Aplicação MEAN Stack Real Time
 * @name Expirou-app
 * @requires NodeJs, AngularJs, MongoDB, Bower, GruntJs
 * @since 10/01/2016
 * @author Vinícius Montanheiro
 * @copyright Coderup
 * @version 1.0.0
 * @licence MIT
 */

(function () {

    /**
     * Dependências
     */
    angular.module('goodfit',
        ['ui.router'
            , 'ngAnimate'
            , 'ngAria'
            , 'toaster'
            , 'ui.bootstrap'
            , 'appServices'
            , 'formDirectives'
        ])
        .controller('AppController', function AppController($scope, ApiService, MessageService, Util) {

            $scope.produto = {};
            $scope.produtos = [];
            $scope.slideshow = [];
            $scope.imagens = [];
            $scope.atual = {};
            $scope.categorias = [];
            var contato = {
                name: '',
                email: '',
                message: '',
                telefone: ''
            };
            $scope.contato = angular.copy(contato);

            $scope.listarProdutos = listarProdutos;
            $scope.detalhar = detalhar;
            $scope.fechar = fechar;
            $scope.popoverShow = popoverShow;
            $scope.popoverHide = popoverHide;

            $scope.$watch("search", function (p) {
                $scope.produto = p;
            });

            var $contactForm = $('#contact-form');

            $('#carousel').carousel({
                interval: 2000
            });

            $(window).scroll(function () {

                var top = 50;

                if ($(".navbar").offset().top > 50) {
                    $(".navbar-fixed-top").addClass("top-nav-collapse");
                    $("#header").addClass("dark-shadow");
                    $("#logo").addClass("logo-resize");
                    $("#bk-header").addClass("bk-header-hide");
                    $('.first-item i').addClass("show");

                    $('a.back-to-top').fadeIn('slow');

                    if ($(".navbar").offset().top > (top * 7)) {
                        $('#carousel .item img').addClass("blur");
                    } else {
                        $('#carousel .item img').removeClass("blur");
                    }

                } else {

                    $('a.back-to-top').fadeOut('slow');

                    $(".navbar-fixed-top").removeClass("top-nav-collapse");
                    $("#header").removeClass("dark-shadow");
                    $("#logo").removeClass("logo-resize");
                    $("#bk-header").removeClass("bk-header-hide");
                    $('.first-item i').removeClass("show");
                    $('.menu li a').removeClass("ativo");
                }
            });

            if ($contactForm.length >= 1) {
                $contactForm.validate({

                    rules: {
                        name: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        message: "required"
                    },
                    messages: {
                        name: "Por favor, informe o nome",
                        email: "Este endereço de e-mail não é válido",
                        message: "Por favor, informe o conteúdo da mensagem"
                    },

                    errorClass: "form-invalid",
                    submitHandler: function (form) {

                        var newContato = $scope.contato;

                        if (Util.fn.isEmpty(newContato, ['name', 'email', 'message'])) {
                            MessageService.addError("Por favor, informe os campos obrigatórios.")
                        } else {
                            ApiService.save("/contactUs", newContato).success(function (res) {
                                MessageService.addSuccess(res.DATA.message);
                                $scope.contato = {};

                            }).error(function (err, status) {
                                MessageService.addError("Verifique sua conexão com a internet!");
                            });
                        }

                    }
                });
            }

            $('a.page-scroll').bind('click', function (event) {
                var $anchor = $(this);
                $('html, body').stop().animate({
                    scrollTop: $($anchor.attr('href')).offset().top
                }, 1500, 'easeInOutExpo');
                event.preventDefault();
            });

            $('#myTabs a').click(function (e) {
                e.preventDefault()
                $(this).tab('show')
            });

            function listarProdutos() {

                ApiService.query("/image/slideshow").success(function (categoria) {

                }).success(function (res) {
                    $scope.slideshow = res.DATA;

                }).error(function (err, status) {
                    MessageService.addError("Por favor,verifique sua conexão com a internet!");
                    console.log(err);
                });


                ApiService.query("/category/list").success(function (categoria) {
                    var categorias = categoria.DATA;

                    if (categorias.length >= 1) {
                        for (var i = 0; i < categorias.length; i++) {
                            ApiService.save("/product/listById", categorias[i]).success(function (produto) {

                                var produtos = produto.DATA.produtos;

                                if (produtos && produtos.length >= 1) {
                                    var obj = {
                                        categoria: {
                                            _id: produto.DATA.categoria._id,
                                            name: produto.DATA.categoria.name,
                                            produtos: produtos[0]
                                        }
                                    };
                                    $scope.produtos = $scope.produtos.concat(produtos[0]);
                                    $scope.categorias.push(obj.categoria);
                                }

                            }).error(function (err, status) {
                                MessageService.addError("Por favor,verifique sua conexão com a internet!");
                                console.log(err);
                            });
                        }
                    }

                }).error(function (err, status) {
                    MessageService.addError("Por favor,verifique sua conexão com a internet!");
                    console.log(err);
                });
            };

            function detalhar(p) {
                removerSlideShow(p.photos);
                $scope.produto = p;
            }

            function popoverShow(e, p) {
                $scope.selectionarTabela(e, p.photos);
            }

            function popoverHide() {
                $scope.tabelaUrl = "";
                $scope.atual.fadeOut(300);

            }

            $scope.selectionarTabela = function (event, photos) {
                for (var i = 0; i < photos.length; i++) {
                    if (photos[i].tnutricional) {
                        $scope.tabelaUrl = photos[i].image.filename;
                        break;
                    }
                }

                if ($scope.tabelaUrl != "") {
                    $scope.atual = $($(event.currentTarget).parent().parent().children()[1]);
                    $scope.atual.fadeIn(600);
                }

            };

            function removerSlideShow(photos) {
                if (photos) {
                    for (var i = 0; i < photos.length; i++) {
                        if (!photos[i].slideshow && photos[i].ativo) {
                            $scope.imagens.push(photos[i]);
                        }
                    }
                }
            }

            function fechar() {
                $scope.search = "";
                $scope.produto = {};
                $scope.imagens = [];
            }

            function animate(el, params, delay, callback) {
                el.animate(params, delay, function () {
                    if (callback) {
                        callback();
                    }
                });
            };

            $('#carousel').carousel({
                interval: 2000
            });

        })
        .config(function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise('/');

            $stateProvider
                .state('/', {
                    url: '/home',
                    templateUrl: 'index.html'
                });

        });

    setTimeout(function () {
        $('.alinhar').addClass('defineRight');
    }, 10000);
    

})();


