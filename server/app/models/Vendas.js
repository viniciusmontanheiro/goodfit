/**
 * Created by vcrzy on 31/01/16.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var History = require('./History');

var vendasSchema = new Schema({
    situation: String,
    description     : String,
    product: { type: Schema.Types.ObjectId, ref: 'produtos' },
    createDate : { type: Date, default: Date.now },
    amount : Number,
    value : Number,
    user :Object
});

History(vendasSchema);
module.exports = mongoose.model('vendas', vendasSchema);
