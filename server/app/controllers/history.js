'use strict';

/**
 * @description Implementação do index controller
 */
var HistoricoProdutos = require('../models/HistoryModel')('produtos_histories');
var HistoricoVendas = require('../models/HistoryModel')('vendas_histories');
var HistoricoImagens = require('../models/HistoryModel')('images_histories');
var HistoricoUsuarios = require('../models/HistoryModel')('users_histories');
var HistoricoCategorias = require('../models/HistoryModel')('categorias_histories');
var HistoricoClientes = require('../models/HistoryModel')('clientes_histories');

var HistoryController = {};

//ListAll
//========================================
HistoryController.products = function(req, res, next){
    var m = new Messages();

    HistoricoProdutos.find(function (err, produtos) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.HANDLEERROR);
        }

        m.GETTED.DATA = produtos;
        return Transporter.json(res, m.GETTED);
    });
};

HistoryController.sales = function(req, res, next){
    var m = new Messages();

    HistoricoVendas.find(function (err, vendas) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.HANDLEERROR);
        }

        m.GETTED.DATA = vendas;
        return Transporter.json(res, m.GETTED);
    });
};

HistoryController.images = function(req, res, next){
    var m = new Messages();

    HistoricoImagens.find(function (err, images) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.HANDLEERROR);
        }

        m.GETTED.DATA = images;
        return Transporter.json(res, m.GETTED);
    });
};

HistoryController.users = function(req, res, next){
    var m = new Messages();

    HistoricoUsuarios.find(function (err, users) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.HANDLEERROR);
        }

        m.GETTED.DATA = users;
        return Transporter.json(res, m.GETTED);
    });
};

HistoryController.categories = function(req, res, next){
    var m = new Messages();

    HistoricoCategorias.find(function (err, categorias) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.HANDLEERROR);
        }

        m.GETTED.DATA = categorias;
        return Transporter.json(res, m.GETTED);
    });
};

HistoryController.clients = function(req, res, next){
    var m = new Messages();

    HistoricoClientes.find(function (err, clientes) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.HANDLEERROR);
        }

        m.GETTED.DATA = clientes;
        return Transporter.json(res, m.GETTED);
    });
};


module.exports = HistoryController;



