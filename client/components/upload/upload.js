'use strict';

angular.module('app.upload', [])
    .controller('UploadController', ['UploadService','$routeParams',
        function (UploadService,$routeParams) {
            var self = this;
            self.up = {};
            self.productId = $routeParams.productId  || null;
            UploadService.upload = [];
            UploadService.dataUrls = [];
            UploadService.selectedFiles = [];
            self.uploadService = UploadService;
            
        }]);