'use strict';

function Messages(){
    return Message;
}



/**
 * Recupera mensagens pré definidas pelo sistema, preparando-as para internacionalização
 * @param TYPE
 * @param CODE
 * @param MESSAGE
 * @param DATA
 * @constructor
 */
var Message = function Message(TYPE, CODE, MESSAGE) {
    this.TYPE = TYPE;
    this.CODE = CODE;
    this.MESSAGE = MESSAGE;
    this.TIMESTAMP = new Date();
    this.DATA = '';
    this.ADDITIONAL = ''
    this.PATH = null;;
};

Message.values = [];
//ERROR
Message.REQUIRED = new Message('error', 500, 'Informe os campos obrigatórios!');
Message.LOGINERROR = new Message('error', 500, 'Usuário ou senha inválidos!');
Message.ACCOUNTERROR = new Message('error', 500, 'Erro ao criar nova conta!');
Message.HANDLEERROR = new Message('error', 500, 'Falha na conexão, tente novamente!');
Message.REMOVEERROR = new Message('error', 500, 'Erro ao remover registro, tente novamente!');
Message.CHANGEERROR = new Message('error', 500, 'Erro ao editar registro, tente novamente!');
Message.CREATEERROR = new Message('error', 500, 'Erro ao criar novo registro!');
Message.USEREXISTS = new Message('error', 500, 'O usuário informado já existe');
Message.GETTEDERROR = new Message('error',500,'Erro ao buscar registro!');
Message.PASSWORDERROR = new Message('error',500,'Senha inválida!');
Message.USERERROR = new Message('error',500,'Usuário inválido!');
Message.DUPLICATE = new Message('error',500,'Erro ao cadastrar : Registro duplicado!');


//SUCCESS
Message.CHANGEDPASS = new Message('success', 200, 'Senha alterada com sucesso!');
Message.CHANGED = new Message('success', 200, 'Dados alterados com sucesso!');
Message.CREATED = new Message('success', 201, 'Novo registro cadastrado!');
Message.GETTED = new Message('success', 200, 'Busca bem sucedida!');
Message.REMOVED = new Message('success', 202, 'Registro removido!');
Message.USERCREATED = new Message('success', 201, 'Novo usuário criado com sucessso!');


module.exports = Messages;