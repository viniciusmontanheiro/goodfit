/**
 * Created by vcrzy on 11/11/15.
 */

'use strict';

var mongoose = require('mongoose');
var History = require('./History');

var imagesSchema = mongoose.Schema({
    ativo : {type: Boolean, "default": true},
    principal: Boolean,
    slideshow: Boolean,
    tnutricional: Boolean,
    image: {
        modificationDate: {type: Date},
        name: {type: String},
        size: {type: Number},
        type: {type: String},
        filename: {type: String}
    },
    createdDate: {type: Date, "default": Date.now}
});

imagesSchema.methods.statusUpdate = function() {
    var query = { _id: this._id };
    var changes = { principal: this.principal};
    this.model("images").update(query, changes,function(err){
        if(err){
            console.log(err);
        }
    });
};

imagesSchema.methods.setSlideShow = function() {
    var query = { _id: this._id };
    var changes = { slideshow: this.slideshow};
    this.model("images").update(query, changes,function(err){
        if(err){
            console.log(err);
        }
    });
};

imagesSchema.methods.setTnutricional = function() {
    var query = { _id: this._id };
    var changes = { tnutricional: this.tnutricional};
    this.model("images").update(query, changes,function(err){
        if(err){
            console.log(err);
        }
    });
};

imagesSchema.methods.ativarImagem = function() {
    var query = { _id: this._id };
    var changes = { ativo: this.ativo};
    this.model("images").update(query, changes,function(err){
        if(err){
            console.log(err);
        }
    });
};

History(imagesSchema);

module.exports = mongoose.model("images", imagesSchema);
