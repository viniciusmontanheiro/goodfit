var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var User = require('../../app/models/User');
var auth = require('./auth');

module.exports = function (passport) {

    var messages = new Messages();

    // Serializa o usuário na sessão
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // Remove o usuário da sessão
    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });

    passport.use('new-account', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, username, password, done) {

            process.nextTick(function () {

                User.findOne({'username': username})
                    .populate('user', '-password')
                    .exec(function (err, user) {

                        if (err) {
                            console.error(err);
                            return done(err);
                        }

                        // Verifica se já existe usuário com esse email
                        if (user) {
                            console.log(err);
                            return done(null,false, req.flash('message', "Usuário ja cadastrado"));
                        } else {

                            if(Util.isEmpty(req.body,['name','username','password'])){
                                return done(null, false, req.flash('message', "Campos inválidos!"));
                            }

                            //Se não, o criamos
                            var newUser = new User();
                            newUser.name = req.body.name;
                            newUser.username = req.body.username;
                            newUser.email = req.body.email;
                            newUser.phone = req.body.phone;
                            newUser.password = newUser.generateHash(password);
                            newUser.active = false;
                            newUser.lastUpdate = Date.now();

                            // Salva o usuário
                            newUser.save(function (err) {
                                if (err) {
                                    console.error(err);
                                }
                                return done(null,newUser);
                            });
                        }
                    });


            });

        }));

    passport.use('admin-login', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, username, password, done) {

            if(username == "" || password == ""){
                return done(null, false, req.flash('entrar', messages.REQUIRED.MESSAGE));
            }

            User.findOne({'username': username})
                .populate('user', '-password')
                .exec(function (err, user) {
                    if (err) {
                        console.error(err);
                        return done(null, false, req.flash('entrar', messages.HANDLEERROR.MESSAGE));
                    }

                    // Se o usuário não for encontrado ou se o password for inválido
                    if (!user || !user.validPassword(password)) {
                        console.error(err);
                        return done(null, false, req.flash('entrar', messages.LOGINERROR.MESSAGE));
                    }
                    console.info("Usuário " + username + " logado!");
                    var usuarioLogado = user._doc;
                    delete usuarioLogado.password;
                    Logged.user = usuarioLogado;
                    done(null, user);
                })
        }));

};