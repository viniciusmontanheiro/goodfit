'use strict';

/**
 * @description Implementação do index controller
 */

var HomeController = {};


//Home
//========================================
HomeController.home = function(req, res, next) {
    var m = new Messages();
    m.GETTED.DATA = {
        titulo: 'Good Fit',
        user : req.user
            ? req.user
            : null

    };


    req.url == "/"
        ? Transporter.render(res,'index',m.GETTED)
        : next();
};


//ListAction - Lista todos os produtos cadastrados
//========================================
HomeController.cadastrar = function(req, res, next){
    var m = new Messages();
    var objMessage = {};

    m.USERCREATED.DATA = {
        titulo: 'Ecomerce Gestão App',
        user : req.user
            ? req.user
            : null
    };

    var errorMessage = req.flash('cadastrar');

    //Recupera mensagem de erro no login
    if(errorMessage != null && errorMessage != undefined && errorMessage.length >= 1){
        m.USERCREATED.CODE = 500;
        m.USERCREATED.MESSAGE = errorMessage;
    }else{
        m.USERCREATED.CODE = 200;
        m.USERCREATED.MESSAGE = 'Usuário criado com sucesso!';
    }

    Transporter.json(res,m.USERCREATED);
};


HomeController.enviarContato = function(req, res, next) {
    var m = new Messages();
    var dest = req.body;
    var mail = new Mail();

    var config = {
        name : 'GMAIL',
        sender : 'goodfitstore@gmail.com',
        auth : 'luh231313',
        headers : {
            from: 'contato@goodfitstore.com.br',
            to: 'luciele@goodfitstore.com.br,lurcard@gmail.com,contato@goodfitstore.com.br',
            subject: 'Novo Contato | Good Fit Store - ' + dest.name,

            html: '<div style="width: 420px;height: 180px;border: solid 8px #ff751a;text-align: left;font-size: 18px;padding:20px;border-style:dotted;margin:auto;margin-top:50px;"> <p><h3 style="text-align:center;"><b>Novo pedido</b></h3></p> <p><b>Nome :</b>'+dest.name+'</p> <p><b>E-mail :</b>'+dest.email+'</p> <p><b>Telefone :</b>'+dest.phone+'</p> <p><b>Mensagem :</b>'+dest.message+'</p></div>'
        },

        error: function error(err) {
            m.CREATEERROR.DATA = {
                message : 'Não foi possível enviar sua mensagem!'
            };
            res.sendStatus(500);
        },
        success : function success() {

            m.CREATED.DATA = {
                message : 'Mensagem enviada com sucesso!'
            };

            Transporter.json(res,m.CREATED);
        }
    };

    mail.send(config);
};

HomeController.entrar = function(req, res, next){
    var m = new Messages();

    m.GETTED.DATA = {
        titulo : 'Good Fit | Acesso ao painel'
    };

    req.url == "/entrar"
        ? Transporter.render(res,'entrar',m.GETTED)
        : next();
};

HomeController.dashboard = function(req, res, next){
    var m = new Messages();

    m.GETTED.DATA = {
        titulo : 'Administração Good Fit Store | Painel',
        user : Logged.user
    };

    req.url == "/dashboard"
        ? Transporter.render(res,'dashboard',m.GETTED)
        : next();
};

module.exports = HomeController;



