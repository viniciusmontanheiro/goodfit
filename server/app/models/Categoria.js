var mongoose = require('mongoose');
var History = require('./History');
var Schema = mongoose.Schema;

// Define o esquema do categoria
var categorias = new mongoose.Schema({
    code:    { type : Number, unique: true, required: true},
    name:     { type : String, unique: true, required: true}
});

History(categorias);
module.exports = mongoose.model('categorias', categorias);
