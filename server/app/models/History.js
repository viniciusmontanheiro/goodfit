/**
 * Created by vcrzy on 01/02/16.
 */

function History(schema,name){

    var Schema = schema;
    var mongoose = require('mongoose');
    var name = name || null;
    var HistoryModel = function(){};
    var attributes = {
        action :'',
        createDate: null,
        entity : {},
        user : {}
    };

    function setAttributes(action,date,entity,user){
        attributes.action = action;
        attributes.createDate = date;
        attributes.entity = entity;
        attributes.user = user;
    };

    Schema.pre('save', function(next) {
        name = name || this.constructor.collection.name + '_histories';
        HistoryModel = require('./HistoryModel')(name);
        setAttributes("CREATED",new Date(),this,Logged.user);
        save(next);
    });

    Schema.pre('update', function(next) {
        name = name || this.model.modelName + '_histories';
        HistoryModel = require('./HistoryModel')(name);
        setAttributes("UPDATED",new Date(),this._update.$set,Logged.user);
        save(next);
    });

    Schema.pre('remove', function(next) {
        name = name || this.constructor.collection.name + '_histories';
        HistoryModel = require('./HistoryModel')(name);
        setAttributes("REMOVED",new Date(),this,Logged.user);
        save(next);
    });

    function save(next){
        var model = new HistoryModel(attributes);
        model.save();
        next();
    };

}

module.exports = History;