'use strict';

angular.module('app.listarcliente', [])
    .controller('ListarclienteController', ['ApiService', 'MessageService','$rootScope','$location','Util','Spinner',
        function (ApiService,MessageService,$rootScope,$location,Util,Spinner) {

            var self = this;
            self.clientes = [];
            self.cliente = {};
            self.showGrid = false;

            self.onClickDelete = onClickDelete;
            self.goToChanges = goToChanges;
            self.init = init;
            self.getUser = getUser;
            self.init();

            function init(){
                Spinner.stop();
                Spinner.start();

                ApiService.query('/client/list').success(function(retorno) {
                    Spinner.stop();
                    self.clientes = retorno.DATA;

                    if(self.clientes && self.clientes.length >=1){
                        self.showGrid = true;
                    }else{
                        self.showGrid = false;
                    }

                }).error(function(data, status) {
                    Spinner.stop();
                    self.showGrid = false;
                    MessageService.addError("Erro ao listar clientes!")
                });
            }

            function onClickDelete(){

                if(!Util.fn.isEmpty(self.cliente,['_id'])){

                    Spinner.start();
                    ApiService.remove('/client/delete/',self.cliente._id).success(function(retorno) {
                        Spinner.stop();
                        MessageService.addSuccess("O cliente "+ self.cliente.name +" foi removido");
                        self.init();
                    }).error(function(data, status) {
                        Spinner.stop();
                        MessageService.addError("Erro ao remover o cliente " + self.cliente.name);
                    });
                }

            }

            function goToChanges(path,cliente) {
                $rootScope.cliente = cliente;
                $location.path(path);
            }

            function getUser(cliente) {
                self.cliente = cliente;
            }

        }]);