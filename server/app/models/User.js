
/**
 * Created by vinicius on 10/04/15.
 */

var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');
var History = require('./History');

// Define o esquema do Usuário

var userSchema = new mongoose.Schema({
    active       : Boolean,
    username     : { type : String, unique: true, required: true},
    name        : {type: String, required: true },
    email       : { type : String, unique: true},
    password    : { type: String, required: true, trim:true },
    phone    : String,
    createDate : { type: Date, default: Date.now },
    lastUpdate: {type: Date}
});

userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

userSchema.set('toJSON', {
    transform: function(doc, ret, options) {
        delete ret.password;
        return ret;
    }
});

History(userSchema);
module.exports = mongoose.model('users', userSchema);
