'use strict';

var pg = require('pg');

/**
 * @description Gerênciador de conexões com postgres
 * @param config
 * @returns {{dao: string, connected: boolean}}
 * @constructor
 */
function PostgresConnector(config) {

    var status = {
        dao: null,
        connected: false,
        uri : "postgres://"
        + config.user + ":"
        + config.pass + "@"
        + config.host + ":"
        + config.port + "/"
        + config.name
    };

    var client = new pg.Client(status.uri);

    console.info("Iniciando conexão com postgres ...");

    //Realiza a conexão
    this.client.connect(function (err) {
        if (err) {
            console.error(new Error('Tentativa de conexão com postgres falhou! \n', err));
        }
        else {
            console.info("Postgres connectado em " + config.name);
            status.connection = client;
            status.connected = true;
        }
    });

    process.on("SIGINT", function () {
        client.end(function () {
            console.info("Postgres! Desconnectado pelo término da aplicação.");
            process.exit(0);
        });
    });

    return status;
};

exports.query = function () {
    return this.status.connection.query;
};

exports.connector = PostgresConnector;


