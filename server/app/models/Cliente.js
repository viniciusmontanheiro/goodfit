
/**
 * Created by vinicius on 10/04/15.
 */

var mongoose = require('mongoose');
var History = require('./History');

// Define o esquema do Cliente
var clienteSchema = new mongoose.Schema({
    description     : String,
    name        : {type: String, required: true },
    email       : { type : String },
    phone    : String,
    createDate : { type: Date, default: Date.now }
});

History(clienteSchema);
module.exports = mongoose.model('clientes', clienteSchema);
