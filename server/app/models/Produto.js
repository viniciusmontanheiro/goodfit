var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var History = require('./History');
require('./Categoria');
require('./Images');

// Define o esquema do Produto
var produtosSchema = new Schema({
    situation: { type: Schema.Types.ObjectId, ref: 'status' },
    code:    { type : String, unique: true, required: true},
    description     : String,
    name        : {type: String, required: true },
    amount       : { type : Number, required: true},
    category: { type: Schema.Types.ObjectId, ref: 'categorias' },
    photos: [{ type: Schema.Types.ObjectId, ref: 'images' }],
    value    : Number,
    createDate : { type: Date, default: Date.now },
    lastUpdate: {type: Date}
});

History(produtosSchema);
module.exports = mongoose.model('produtos', produtosSchema);
