'use strict';

angular.module('app.estoque', [])
    .controller('EstoqueController', ['ApiService', 'MessageService','$rootScope','$location','Util','Spinner',
        function (ApiService,MessageService,$rootScope,$location,Util,Spinner) {
                var self = this;
                self.products = [];
                self.isDescont = false;
                self.product = {
                        sold :{
                                amount : 0,
                                description :'',
                                percent : 0
                        }
                };
                self.showGrid = false;
                self.parseInt = parseInt;

                self.onClickDelete = onClickDelete;
                self.onClickRemove = onClickDelete;
                self.goToChanges = goToChanges;
                self.init = init;
                self.getProduct = getProduct;
                self.goToPage = goToPage;
                self.atualizarStatus = atualizarStatus;
                self.atualizarDestaque = atualizarDestaque;
                self.init();

                function init(){
                        Spinner.stop();
                        Spinner.start();

                        ApiService.query('/product/list').success(function(retorno) {
                                Spinner.stop();
                                self.products = retorno.DATA;

                                if(self.products && self.products.length >= 1){
                                        self.showGrid = true;
                                }else{
                                        self.showGrid = false;
                                }

                        }).error(function(data, status) {
                                Spinner.stop();
                                self.showGrid = false;
                                MessageService.addError("Erro ao listar produtos!")
                        });
                }

                function onClickDelete(){

                        if(!Util.fn.isEmpty(self.product,['_id'])){

                                Spinner.start();

                                ApiService.remove('/product/delete/',self.product._id).success(function(retorno) {
                                        Spinner.stop();
                                        MessageService.addSuccess("O Produto "+ self.product.name +" foi removido!");
                                        self.init();
                                }).error(function(data, status) {
                                        Spinner.stop();
                                        MessageService.addError("Erro ao remover o produto " + self.product.name);
                                });
                        }

                }

                function atualizarStatus(){
                        self.product.situation.status = "VENDIDO";

                        if(Util.fn.isEmpty(self.product.sold,['description','amount'])){
                                return MessageService.addError("Verifique os campos obrigatórios.");
                        }else{
                               if(self.isDescont && Util.fn.isEmpty(self.product.sold,['percent'])){
                                       return MessageService.addError("Verifique os campos obrigatórios.");
                               }
                        }

                        Spinner.start();

                        ApiService.update('/product/update/status',self.product).success(function(retorno) {
                                Spinner.stop();
                                MessageService.addSuccess("Venda realizada com sucesso!");
                                self.init();

                        }).error(function(data, status) {
                                Spinner.stop();
                                MessageService.addError("Erro ao realizar venda!");
                        });
                }

                function atualizarDestaque(item){
                        var mensagem = "";

                        if(item.situation.status == "EM DESTAQUE"){
                                item.situation.status = "ATIVO";
                                mensagem = "Produto removido dos destaques!";
                        }else{
                                item.situation.status = "EM DESTAQUE"
                                mensagem = "Produto adicionado aos destaques!";
                        }

                        Spinner.start();

                        ApiService.update('/product/update/destaque',item).success(function(retorno) {
                                Spinner.stop();
                                MessageService.addSuccess(mensagem);
                                self.init();

                        }).error(function(data, status) {
                                Spinner.stop();
                                MessageService.addError("Erro ao atualizar status!");
                        });
                }


                function goToChanges(path,product) {
                        $rootScope.product = product;
                        $location.path(path);
                }

                function getProduct(product) {
                        self.product = product;
                }

                function goToPage(url,product) {
                        $location.path(url + product._id);
                }

                function parseInt(valor){
                        return parseInt(valor);
                }
        }]);