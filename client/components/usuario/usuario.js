'use strict';

angular.module('app.usuario', [])
    .controller('UsuarioController', ['ApiService', 'MessageService','Util','$rootScope','$routeParams','$location','Spinner',
        function (ApiService,MessageService,Util,$rootScope,$routeParams,$location,Spinner) {

                var self = this;
                self.isUpdate = false;
                self.model = {
                        name : '',
                        username:'',
                        email:'',
                        phone:'',
                        password : ''
                };

                if($routeParams.req == "cadastrar"){
                        self.isUpdate = false;
                        delete $rootScope.user;
                }else{
                    setTimeout(function(){
                            $('#password').removeAttr("required");
                    },1500);
                }

                self.onClick = function(){};
                self.goToList = goToList;

                function onClickCadastrar(){

                        if(Util.fn.isEmpty(self.model,['username','password','phone'])){
                                return MessageService.addError("Verifique os campos obrigatórios.");
                        }

                        Spinner.start();

                        ApiService.save("/user/add",self.model)
                            .success(function(retorno){
                                    Spinner.stop();
                                self.model = {};
                                MessageService.addSuccess("Novo usuário criado com sucesso");
                                    self.goToList('/list');
                        }).error(function(err){
                                Spinner.stop();
                                if(err){
                                        MessageService.addError(err.MESSAGE);
                                }
                        });
                }

                function onClickChanges(){

                        Spinner.start();

                        ApiService.update('/user/update',self.model).success(function(retorno) {
                                Spinner.stop();
                                delete $rootScope.user;
                                MessageService.addSuccess("Usuário alterarado com sucesso!");
                                self.goToList('/listar');

                        }).error(function(data, status) {
                                Spinner.stop();
                                MessageService.addError("Erro ao alterar usuário!");
                        });
                }

                if(!Util.fn.isEmpty($rootScope,['user'])){
                        var user = $rootScope.user;
                        self.model._id = user._id;
                        self.model.name = user.name;
                        self.model.username = user.username;
                        self.model.phone = user.phone;
                        self.model.email = user.email;
                        self.onClick = onClickChanges;
                        self.isUpdate = true;
                }else{
                        self.onClick  = onClickCadastrar;
                        self.isUpdate = false;
                }

                function goToList(path) {
                        $location.path(path);
                }

        }]);