'use strict';

var log4js = require('log4js');

/**
 * @description Configurações do sistema de log
 */
log4js.configure({
    "appenders": [
        {
            type: "console"
        },

        {
            "type": "dateFile",
            "filename": __dirname + "/access.log",
            "pattern": "-yyyy-MM-dd",
            "category": "ALL"
        },

        {
            "type": "file",
            "filename": __dirname + "/app.log",
            "maxLogSize": 10485760,
            "numBackups": 3
        },

        {
            "type": "logLevelFilter",
            "level": "ERROR",
            "appender": {
                "type": "file",
                "filename": __dirname + "/errors.log"
            }
        }
    ],
    replaceConsole: true
});

/**
 * Seta log como modo geral
 * @type {Logger}
 */
var log = log4js.getLogger("ALL");
