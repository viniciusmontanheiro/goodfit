'use strict';

/**
 * Aplicação MEAN Stack Real Time
 * @name Expirou-app
 * @requires NodeJs, AngularJs, MongoDB, Bower, GruntJs
 * @since 10/01/2016
 * @author Vinícius Montanheiro, Antony Alkmim
 * @copyright Coderup
 * @version 1.0.0
 * @licence MIT
 */

(function() {

    /**
     * Dependências
     */
    angular.module('ecommerce-admin',
        ['ngNewRouter'
            ,'ngAnimate'
            ,'ngAria'
            ,'ui.bootstrap'
            ,'angularSpinner'
            ,'toaster'
            ,'appServices'
            ,'formDirectives'
            ,'angularFileUpload'
            ,'app.dashboard'
            ,'app.produto'
            ,'app.estoque'
            ,'app.listar'
            ,'app.categoria'
            ,'app.upload'
            ,'app.image'
            ,'app.venda'
            ,'app.cliente'
            ,'app.listarcliente'
            ,'app.usuario'

        ])
        .controller('AppController', AppController)
        .config(function($locationProvider) {
        });

    AppController.$inject = [
        '$rootScope'
        ,'usSpinnerService'
    ];

    //Rotas
    AppController.$routeConfig = [
        { path: '/', component: 'dashboard' },
        { path: '/dashboard', component: 'dashboard' },
        { path: '/produto/:req', component: 'produto' },
        { path: '/estoque', component: 'estoque' },
        { path: '/usuario/:req', component: 'usuario' },
        { path: '/listar', component: 'listar' },
        { path: '/categoria', component: 'categoria' },
        { path: '/upload/:productId', component: 'upload' },
        { path: '/image/:productId', component: 'image' },
        { path: '/venda', component: 'venda' },
        { path: '/cliente/:req', component: 'cliente' },
        { path: '/listarcliente',component:'listarcliente'},

    ];

    //Main controller
    function AppController($rootScope) {

        $.material.init();
        $.material.ripples();
        $.material.input();
        $.material.checkbox();
        $.material.radio();
        
    };

})();


