'use strict';

/**
 * @description Implementação do VendaController
 */

var Venda = require("../models/Vendas");
var Produto = require("../models/Produto");
var Status = require("../models/Status");
var VendasController = {};

//ListAction - Lista todos os Vendas cadastrados
//========================================
VendasController.list = function (req, res, next) {
    var m = new Messages();

    Venda.find({})
        .populate('product')
        .populate('owner')
        .exec(function (err, Vendas) {
            m.GETTED.DATA = Vendas;
            return Transporter.json(res, m.GETTED);
        });
};

//CreateAction
//========================================
VendasController.create = function (req, res, next) {
    var m = new Messages();
    var Venda = new Venda(req.body);
    var situation = {
        status: req.body.situation || 'ATIVO'
    };

    var promise = Status.create(situation);

    promise.then(function (newSituation) {
        var m = new Messages();
        Venda.situation = newSituation._id;

        Venda.save(function (err) {
            if (err) {
                return Transporter.json(res, m.CREATEERROR);
            }
            return Transporter.json(res, m.CREATED);
        });
    });
};


//UpdateAction
//========================================
VendasController.update = function (req, res, next) {
    var m = new Messages();
    var Venda = req.body;

    if (Util.isEmpty(Venda, ['_id'])) {
        return Transporter.json(res, m.CHANGEERROR);
    }

    var id = Venda._id;
    delete Venda._id;

    Venda.update({_id: id}, Venda, {upsert: true}, function (err) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.CHANGEERROR);
        }
        return Transporter.json(res, m.CHANGED);
    });

};


//RemoveAction
//========================================
VendasController.delete = function (req, res, next) {
    var m = new Messages();
    var id = req.params.id;

    Venda.find({_id:id})
        .populate('product')
        .exec(function (err, venda) {
          var sale = venda[0]._doc;
          var product = sale.product._doc;

            if(Util.isEmpty(product,['_id'])){
                return Transporter.json(res, m.REMOVEERROR)
            }

            var product_id = product._id;
            product.amount = parseInt(product.amount + sale.amount);
            delete product._id;

            Produto.findOne({_id:product_id})
                .populate('situation')
                .exec(function(err,s){
                if(err){
                    console.error(err);
                }
                product.situation = s._doc.situation._doc;

                    if(product.amount > 0){
                        product.situation.status = 'ATIVO';
                        var status = new Status(product.situation);
                        status.statusUpdate();
                    }
            });

            Produto.update({_id: product_id}, product, {upsert: true}, function (err) {
                if (err) {
                    console.error(err);
                    return Transporter.json(res, m.REMOVEERROR);
                }

                Venda.findOne({_id: id},function (err,retorno) {
                    if (err) {
                        console.error(err);
                        return Transporter.json(res, m.REMOVEERROR);
                    }

                    retorno.remove(function(err){
                        if(err){
                            return Transporter.json(res, m.REMOVEERROR);
                        }
                        return Transporter.json(res, m.REMOVED);
                    });

                });
            });

        });
};

module.exports = VendasController; 




