'use strict';

/**
 * @description Configurações do servidor
 * @author Vinícius Montanheiro
 */

var http = require('http');
var express = require('express');
var flash    = require('connect-flash');
var methodOverride = require('method-override');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var multer = require('multer');
var passport = require('passport');
var session = require('express-session');
var helmet = require('helmet');
var Routes = require('./../../app/routes.js');
var socketio = require('socket.io');
var Auth = require('../oAuth/passport.js');
var configs = global.configs;
var path = require('path');

//Disponibilizando o app na aplicação
GLOBAL.app = express();

function Server() {

    var root = path.normalize(__dirname + '/../../..');

    //Configurações do express
    app.set("port", appConfig.port);
    app.use(cookieParser());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());
    app.use(methodOverride());
    app.use(multer());
    app.set('views', root + '/client/app');
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');
    app.use(express.static(path.join(root, '/client')));
    app.use(helmet());
    app.use(session({
        secret: 'teqdegsxzy-www checz',
        resave: true,
        saveUninitialized: true
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(flash());

    //Configurações de segurança
    app.use(helmet.xframe()); // Nenhum iframe pode referênciar essa aplicação
    app.use(helmet.xssFilter()); // Proteção contra ataques xss
    app.use(helmet.nosniff()); // Não permite que o browser modifique o MIME TYPE
    app.disable('x-powered-by'); // Removendo informações da tecnologia usada
    app.use(helmet.hidePoweredBy({setTo: 'PHP 5.5'})); // Atribui informações falsas ao powered by

    //Rotas
    Routes(passport);

    //oAuth
    Auth(passport);

    // Estabelecendo servidor
    var server = http.createServer(app);

    // Iniciando servidor
    server.listen(app.get("port"), function (err) {
        if (err) {
            console.error(new Error(':-( Server DOWN!!'), err);
        }
        console.info('INFO: ' + JSON.stringify(appConfig.actived));
        console.info('Memória:' + JSON.stringify(process.memoryUsage()));
        console.info('Servidor iniciado na porta %d', app.get("port"));

        //Verifica o uso do banco de dados
        if(appConfig.actived.databases){
            var FactoryDatabases = require("../database/factoryDatabases");
            GLOBAL.Connections = new FactoryDatabases().getConnections();
        }

        //Verifica o uso de sockets
        if(appConfig.actived.socket){
            // Instanciando socket
            var io = socketio(server);
            // Disponibilizando socket na aplicação
            GLOBAL.Socket = io;
        }
    });
};
module.exports = Server;









