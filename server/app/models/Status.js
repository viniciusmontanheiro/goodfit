var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//STATUS ENUM
var statusSchema = new Schema({
	status       : { type: String,enum: ['INATIVO', 'ATIVO', 'VENDIDO','EXCLUIDO','FORA DE ESTOQUE','EM DESTAQUE'],es_indexed: true}
});

statusSchema.methods.statusUpdate = function() {
	var query = { _id: this._id };
	var changes = { status: this.status};
	this.model("status").update(query, changes,function(err){
		if(err){
			console.log(err);
		}
	});
};

module.exports = mongoose.model('status', statusSchema);
