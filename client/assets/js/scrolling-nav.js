var $login = $('.box-login');
var $loading = $('.loading');
var $formLogin = $("#login-form");

$(function() {

    $.material.init();
    $.material.ripples();
    $.material.input();
    $.material.checkbox();
    $.material.radio();

});

if($formLogin.length >= 1){
    $formLogin.validate({

        rules: {
            username: {
                required: true,
                minlength: 4
            },
            password: {
                required: true,
                minlength: 6
            }
        },

        messages: {
            username: {
                required: "Informe o nome de usuário",
                minlength: "O nome de usuário é maior que 4 caracteres"
            },
            password: {
                required: "Informe sua senha",
                minlength: "Sua senha deve ser maior que 6 caracteres"
            }
        },
        errorClass: "form-invalid",
        submitHandler: function(form) {
            animate($login, {
                top: '-2000px'
            },800,function(){
                animate($loading,{
                    'opacity' : 1
                },300,function(){
                    form.submit();
                });
            });
        }
    });
}

function animate(el,params,delay,callback){
    el.animate(params,delay,function(){
        if(callback){
            callback();
        }
    });
};

$(window).load(function(){

    animate($login, {
        top: '280px'
    },800,function(){
        animate($loading,{
            'opacity' : 0
        },300,null)
    });
});



