'use strict';

angular.module('app.categoria', [])
    .controller('CategoriaController', ['ApiService', 'MessageService','Util','Spinner',
        function (ApiService,MessageService,Util,Spinner) {

            var self = this;
            self.isUpdate = false;
            self.categories = [];
            self.category = {};
            self.showGrid = false;
            self.model = {
                code : null,
                name : ''
            };

            self.init = init;
            self.onClickChanges = onClickChanges;
            self.onClickDelete = onClickDelete;
            self.onClickCadastrar = onClickCadastrar;
            self.getCategory = getCategory;
            self.goToChanges = goToChanges;
            self.init();

            function init(){

                Spinner.stop();
                Spinner.start();

                self.isUpdate = false;

                ApiService.query('/category/list').success(function(retorno) {
                    Spinner.stop();
                    self.categories = retorno.DATA;

                    if(self.categories && self.categories.length >=1){
                        self.showGrid = true;
                    }else{
                        self.showGrid = false;
                    }

                }).error(function(data, status) {
                    Spinner.stop();
                    self.showGrid = false;
                    MessageService.addError("Erro ao listar categorias");
                });
            }


            function onClickCadastrar(){



                if(self.model.code < 0 || self.model.name == "" ){
                    return MessageService.addError("Verifique os campos obrigatórios.");
                }

                Spinner.start();

                ApiService.save("/category/create",self.model)
                    .success(function(retorno){
                        Spinner.stop();
                        self.model = {};
                        MessageService.addSuccess("Nova categoria cadastrada com sucesso");
                        self.init();
                    }).error(function(err){
                    if(err){
                        Spinner.stop();
                        MessageService.addError(err.MESSAGE);
                    }
                });
            }

            function onClickChanges(){
                Spinner.start();

                ApiService.update('/category/update',self.model).success(function(retorno) {
                    Spinner.stop();
                    MessageService.addSuccess("Categoria alterarada com sucesso!");
                    self.model = {};
                    self.init();
                }).error(function(data, status) {
                    Spinner.stop();
                    MessageService.addError("Erro ao alterar o categoria!");
                });
            }

            function onClickDelete(){
                if(!Util.fn.isEmpty(self.category,['_id'])){
                    Spinner.start();

                    ApiService.remove('/category/delete/',self.category._id).success(function(retorno) {
                        Spinner.stop();
                        MessageService.addSuccess("A categoria "+ self.category.name +" foi removido");
                        self.init();
                    }).error(function(data, status) {
                        Spinner.stop();
                        MessageService.addError("Erro ao remover o categoria " + self.category.name);
                    });
                }

            }

            function getCategory(category){
                self.category = category;
            }

            function goToChanges(category){
                self.model._id = category._id;
                self.model.code = category.code;
                self.model.name = category.name;
                self.isUpdate = true;
            }

        }]);