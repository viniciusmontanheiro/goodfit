/**
 * Created by vcrzy on 31/01/16.
 */
'use strict';

angular.module('app.venda', [])
    .controller('VendaController', ['ApiService', 'MessageService','Util','Spinner',
        function (ApiService,MessageService,Util,Spinner) {
            var self = this;
            self.sales = [];
            self.sale = {};
            self.total = 0;
            self.saleSearch = '';
            self.showGrid = false;
            self.getVenda = getVenda;
            self.onClickDelete = onClickDelete;
            self.getTotal = getTotal;
            self.init = init;
            self.init();

            function init(){

                Spinner.stop();
                Spinner.start();

                ApiService.query('/sales/list').success(function(retorno) {
                    Spinner.stop();
                    self.sales = retorno.DATA;

                    if(self.sales && self.sales.length >=1){
                        self.showGrid = true;
                        self.total = self.getTotal(self.sales);
                    }else{
                        self.showGrid = false;
                    }

                }).error(function(data, status) {
                    Spinner.stop();
                    self.showGrid = false;
                    MessageService.addError("Erro ao listar vendas!")
                });
            }

            function onClickDelete(){

                if(!Util.fn.isEmpty(self.sale,['_id'])){

                    Spinner.start();

                    ApiService.remove('/sales/delete/',self.sale._id).success(function(retorno) {
                        Spinner.stop();
                        MessageService.addSuccess("A venda do produto foi cancelada");
                        self.init();
                    }).error(function(data, status) {
                        Spinner.stop();
                        MessageService.addError("Erro ao cancelar venda");
                    });
                }

            }

            function getVenda(sale) {
                self.sale = sale;
            }

             function getTotal(list){
                var total = 0;
                for(var i = 0; i < list.length; i++){
                    total += list[i].value;
                }
                return total;
            }

        }]);