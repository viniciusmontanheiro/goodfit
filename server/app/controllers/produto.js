'use strict';

/**
 * @description Implementação do ProdutoController
 */

var Produto = require("../models/Produto");
var Status = require("../models/Status");
var Venda = require("../models/Vendas");
var Categoria = require("../models/Categoria");
var ProdutoController = {};

//ListAction - Lista todos os produtos cadastrados
//========================================
ProdutoController.list = function (req, res, next) {
    var m = new Messages();

    Produto.find({})
        .populate('category')
        .populate('situation')
        .populate('photos')
        .exec(function (err, produtos) {
            m.GETTED.DATA = produtos;
            return Transporter.json(res, m.GETTED);
        });
};

ProdutoController.listById = function (req, res, next) {
    var m = new Messages();
    var categoria = new Categoria(req.body);
    var obj = {};
    var arrProdutos = [];

    Produto.find({'category': categoria})
        .populate('category')
        .populate('situation')
        .populate('photos')
        .exec(function (err, produtos) {

            obj = produtos;

            if(obj && obj.length >=1){
                arrProdutos.push(obj);
            }

            m.GETTED.DATA = {
                categoria: {
                    _id: req.body._id,
                    name: req.body.name
                },
                produtos: arrProdutos
            };
            return Transporter.json(res, m.GETTED);
        });
};

//CreateAction
//========================================
ProdutoController.create = function (req, res, next) {
    var m = new Messages();
    var produto = new Produto(req.body);
    var situation = {
        status: req.body.situation || 'ATIVO'
    };

    var promise = Status.create(situation);

    promise.then(function (newSituation) {
        var m = new Messages();
        produto.situation = newSituation._id;

        produto.save(function (err) {
            if (err) {
                return Transporter.json(res, m.CREATEERROR);
            }
            return Transporter.json(res, m.CREATED);
        });
    });
};


//UpdateAction
//========================================
ProdutoController.update = function (req, res, next) {
    var m = new Messages();
    var produto = req.body;

    if (Util.isEmpty(produto, ['_id'])) {
        return Transporter.json(res, m.CHANGEERROR);
    }

    var id = produto._id;
    delete produto._id;

    if (produto.amount > 0) {
        produto.situation.status = 'ATIVO';
        var status = new Status(produto.situation);
        status.statusUpdate();
    }

    Produto.update({_id: id}, produto, {upsert: true}, function (err) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.CHANGEERROR);
        }
        return Transporter.json(res, m.CHANGED);
    });

};

//UpdateAction
//========================================
ProdutoController.statusUpdate = function (req, res, next) {
    var m = new Messages();
    var produto = req.body;

    if (Util.isEmpty(produto, ['_id'])) {
        return Transporter.json(res, m.CHANGEERROR);
    }

    var situation = produto.situation;

    if (situation.status == "VENDIDO") {
        var valor = produto.value || 0;
        var percent = parseInt(produto.sold.percent);

        if (produto.sold.amount > 1) {
            valor = valor * produto.sold.amount;
        }

        if (valor > 0) {
            if (percent > 0) {
                var resultado = (valor / 100);
                valor = valor - (percent * resultado);
            }
        } else {
            m.CHANGED.MESSAGE = "Informe o valor do produto para realizar a venda!";
            return Transporter.json(res, m.CHANGED);
        }

        produto.amount = produto.amount > produto.sold.amount
            ? produto.amount - produto.sold.amount
            : produto.sold.amount - produto.amount;

        var novaVenda = {
            situation: situation.status,
            description: produto.sold.description,
            product: produto,
            amount: produto.sold.amount,
            value: valor,
            user: Logged.user
        };
        Venda.create(novaVenda);

        if (produto.amount <= 0) {
            situation.status = "FORA DE ESTOQUE";
            var status = new Status(situation);
            produto.situation = situation;
            status.statusUpdate();
        } else {
            situation.status = "ATIVO";
            var status = new Status(situation);
            produto.situation = situation;
            status.statusUpdate();
        }
    } else {

        var status = new Status(situation);
        produto.situation = situation;
        status.statusUpdate();

    }

    var id = produto._id;
    delete produto._id;

    Produto.update({_id: id}, produto, {upsert: true}, function (err) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.CHANGEERROR);
        }
        return Transporter.json(res, m.CHANGED);
    });

};

//RemoveAction
//========================================
ProdutoController.delete = function (req, res, next) {
    var m = new Messages();
    var id = req.params.id;

    Produto.findOne({_id: id}, function (err, retorno) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.REMOVEERROR);
        }

        retorno.remove(function (err) {
            if (err) {
                return Transporter.json(res, m.REMOVEERROR);
            }
            return Transporter.json(res, m.REMOVED);
        });

    });
};

//Listar Categorias de Produtos
//========================================
ProdutoController.listCategorias = function (req, res, next) {
    var m = new Messages();

};


module.exports = ProdutoController; 




