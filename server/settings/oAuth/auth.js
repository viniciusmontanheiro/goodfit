module.exports = {

    'facebookAuth': {
        'clientID': '465254650345438',
        'clientSecret': 'f9c306d85a314074bcf0cca58a1add1d',
        'callbackURL': 'http://expirou.com.br/manager'
    },

    'twitterAuth': {
        'consumerKey': 'your-consumer-key-here',
        'consumerSecret': 'your-client-secret-here',
        'callbackURL': 'http://localhost:8080/auth/twitter/callback'
    },

    'googleAuth': {
        'clientID': 'your-secret-clientID-here',
        'clientSecret': 'your-client-secret-here',
        'callbackURL': 'http://localhost:8080/auth/google/callback'
    }

};
