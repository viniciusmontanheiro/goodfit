'use strict';

angular.module('app.produto', [])
    .controller('ProdutoController', ['ApiService', 'MessageService','Util','$rootScope','$routeParams','$location','Enum','Spinner',
        function (ApiService,MessageService,Util,$rootScope,$routeParams,$location,Enum,Spinner) {

                var self = this;
                self.isUpdate = false;
                self.categories = [];
                self.disabled = false;
                var model = {
                        situation:'',
                        code : '',
                        name : '',
                        category: null,
                        description:'',
                        amount:0,
                        value : 0
                };

                self.model = angular.copy(model);

                if($routeParams.req == "cadastrar"){
                        self.isUpdate = false;
                        delete $rootScope.product;
                }

                self.onClick = function(){};
                self.goToList = goToList;
                self.init = init;
                self.init();

                function init(){
                        Spinner.stop();
                        Spinner.start();

                        ApiService.query('/category/list').success(function(retorno) {
                                Spinner.stop();
                                self.categories = retorno.DATA;
                        }).error(function(data, status) {
                                Spinner.stop();
                                MessageService.addError("Erro ao listar categorias")
                        });
                }


                function onClickCadastrar(){



                        var category = {};
                        category.situation = 'ATIVO';
                        category.code = self.model.code;
                        category.name = self.model.name;
                        category.category = self.model.category;
                        category.description = self.model.description;
                        category.amount = self.model.amount || 0;
                        category.value = self.model.value || 0;

                        if(Util.fn.isEmpty(category,['code','name','category','amount'])){
                                return MessageService.addError("Verifique os campos obrigatórios.");
                        }

                        Spinner.start();

                        ApiService.save("/product/create",category)
                            .success(function(retorno){
                                    Spinner.stop();
                                    self.model = angular.copy(model);
                                    MessageService.addSuccess("Novo produto criado com sucesso");

                            }).error(function(err){
                                Spinner.stop();
                                if(err){
                                        MessageService.addError(err.MESSAGE);
                                }
                        });
                }

                function onClickChanges(){



                        if(Util.fn.isEmpty(self.model,['category'])){
                                return MessageService.addError("Informe a categoria!");
                        }

                        Spinner.start();

                        ApiService.update('/product/update',self.model).success(function(retorno) {
                                Spinner.stop();
                                delete $rootScope.product;
                                MessageService.addSuccess("Produto alterarado com sucesso!");
                                self.goToList('/estoque');

                        }).error(function(data, status) {
                                Spinner.stop();
                                MessageService.addError("Erro ao alterar o produto!");
                        });
                }

                if(!Util.fn.isEmpty($rootScope,['product'])){
                        var product = $rootScope.product;
                        self.model._id = product._id;
                        self.model.name = product.name;
                        self.model.code = product.code;
                        self.model.category = product.category;
                        self.model.description = product.description;
                        self.model.value = product.value;
                        self.model.amount = product.amount;
                        self.model.situation = product.situation;
                        self.onClick = onClickChanges;
                        self.isUpdate = true;

                }else{
                        self.onClick  = onClickCadastrar;
                        self.isUpdate = false;
                }

                function goToList(path) {
                        $location.path(path);
                }

               


        }]);