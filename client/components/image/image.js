/**
 * Created by vcrzy on 29/01/16.
 */

'use strict';

angular.module('app.image', [])
    .controller('ImageController', ['ApiService', 'MessageService','$routeParams','$location','Util','Spinner','$timeout',
        function (ApiService,MessageService,$routeParams,$location,Util,Spinner,$timeout) {

            var self = this;
            self.productId = $routeParams.productId || null;
            self.photos = [];
            self.image = {};
            self.getImage = getImage;
            self.onClickDelete = onClickDelete;
            self.back = back;
            self.init = init;
            self.atualizarImagemPrincipal = atualizarImagemPrincipal;
            self.atualizarImagemSlide = atualizarImagemSlide;
            self.definirTabelaNutricional = definirTabelaNutricional;
            self.ativarImagem = ativarImagem;
            self.init();

            function init(){

                Spinner.stop();
                Spinner.start();

                if(self.productId != null){

                    ApiService.findBy('/image/list/',self.productId).success(function(retorno) {
                        Spinner.stop();
                        self.photos = retorno.DATA;
                    }).error(function(data, status) {
                        Spinner.stop();
                        MessageService.addError("Erro ao listar imagens!")
                    });
                }
            }

            function onClickDelete(){
                if(!Util.fn.isEmpty(self.image,['_id'])){

                    Spinner.start();

                    ApiService.remove('/image/delete/',self.image._id).success(function(retorno) {
                        Spinner.stop();
                        MessageService.addSuccess("Imagem removida");
                        self.init();
                    }).error(function(data, status) {
                        Spinner.stop();
                        MessageService.addError("Erro ao remover o imagem!");
                    });
                }

            }

            //function showImage(e){
            //    var content = $('.modal-body');
            //    content.empty();
            //    content.html($(e.currentTarget).children().css({'width':'100%','height':'','max-height':'400px'}));
            //    $(".modal-profile").modal({show:true});
            //}

            function getImage(image) {
                self.image = image;
            }

            function back() {
                $location.path("/estoque");
            }

            function atualizarImagemPrincipal(item){

                Spinner.start();
                item.principal = !item.principal;
                ApiService.update('/image/main',item).success(function() {
                    Spinner.stop();
                    MessageService.addSuccess("A imagem foi definida como princial!");

                }).error(function(data, status) {
                    Spinner.stop();
                    item.principal = !item.principal;
                    MessageService.addError("Erro ao definir imagem principal!");
                });
            }

            function atualizarImagemSlide(item){

                Spinner.start();

                item.slideshow = !item.slideshow;
                ApiService.update('/image/setSlideShow',item).success(function() {
                    Spinner.stop();
                    MessageService.addSuccess("A imagem foi adicionada ao slideshow!");

                }).error(function(data, status) {
                    Spinner.stop();
                    item.slideshow = !item.slideshow;
                    MessageService.addError("Erro ao adicionar imagem ao slideshow!");
                });
            }

            function definirTabelaNutricional(item){

                Spinner.start();

                item.tnutricional = !item.tnutricional;
                ApiService.update('/image/setTnutricional',item).success(function() {
                    Spinner.stop();
                    MessageService.addSuccess("A imagem foi adicionada a tabela nutricional!");

                }).error(function(data, status) {
                    Spinner.stop();
                    item.tnutricional = !item.tnutricional;
                    MessageService.addError("Erro ao adicionar imagem  a tabela nutricional!");
                });
            }

            function ativarImagem(item){

                Spinner.start();

                item.ativo = !item.ativo;
                ApiService.update('/image/ativarImagem',item).success(function() {
                    Spinner.stop();
                    MessageService.addSuccess("A imagem foi ativada!");

                }).error(function(data, status) {
                    Spinner.stop();
                    item.ativo = !item.ativo;
                    MessageService.addError("Erro ao ativar imagem!");
                });
            }

           


          
        }]);

