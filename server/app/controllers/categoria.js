'use strict';

/**
 * @description Implementação do index controller
 */

var Categoria = require("../models/Categoria");
var Produto = require("../models/Produto");

var CategoriaController = {};

//ListAll
//========================================
CategoriaController.list = function(req, res, next){
    var m = new Messages();

    Categoria.find({})
        .sort("code")
        .exec(function (err, categorias) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.HANDLEERROR);
        }

        m.GETTED.DATA = categorias;
        return Transporter.json(res, m.GETTED);
    });
};


//CreateAction
//========================================
CategoriaController.create = function(req, res, next){
    var m = new Messages();
    var categoria = new Categoria(req.body);

    categoria.save(function(err,data){
        if(err){

            if(err.code = 11000){
                return Transporter.json(res, m.DUPLICATE);
            }
            return Transporter.json(res, m.CREATEERROR);

        }
        return Transporter.json(res, m.CREATED);
    });
};

//Update
//========================================
CategoriaController.update = function(req, res, next){
    var m = new Messages();
    var categoria = req.body;

    if (Util.isEmpty(categoria,['_id'])) {
        return Transporter.json(res, m.CHANGEERROR);
    }

    var id = categoria._id;
    delete categoria._id;

    Categoria.update({_id: id}, categoria, {upsert: true}, function (err) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.CHANGEERROR);
        }
        return Transporter.json(res, m.CHANGED);
    });
};

//Delete
//========================================
CategoriaController.delete = function(req, res, next){
    var m = new Messages();
    var id = req.params.id;

    Categoria.findOne({_id: id},function (err,retorno) {
        if (err) {
            console.error(err);
            return Transporter.json(res, m.REMOVEERROR);
        }

        retorno.remove(function(err){
            if(err){
                return Transporter.json(res, m.REMOVEERROR);
            }
            return Transporter.json(res, m.REMOVED);
        });

    });
};


module.exports = CategoriaController;



