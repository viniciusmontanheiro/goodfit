/**
 * Unifica variáveis globais da aplicação
 * @type {Util|exports|module.exports}
 */

// Módulos
GLOBAL.clone = require("clone");

var EventEmitter = require('events').EventEmitter;
GLOBAL.Event = new EventEmitter();

var Util = require("../../commons/utils/util");
GLOBAL.Util = new Util();

var Mail = require("../../commons/mail/mailer");
GLOBAL.Mail = Mail;

var Transporter = require("../notifications/transporter");
GLOBAL.Transporter = new Transporter();

var Messages = require("../notifications/messages");
GLOBAL.Messages = Messages;

GLOBAL.Logged = {
    user : {}
};



