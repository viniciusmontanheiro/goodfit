'use strict';

angular.module('app.cliente', [])
    .controller('ClienteController', ['ApiService', 'MessageService','Util','$rootScope','$routeParams','$location','Spinner',
        function (ApiService,MessageService,Util,$rootScope,$routeParams,$location,Spinner) {

            var self = this;
            self.isUpdate = false;
            self.model = {
                name : '',
                description:'',
                email:'',
                phone:''
            };

            if($routeParams.req == "cadastrar"){
                self.isUpdate = false;
                delete $rootScope.cliente;
            }

            self.onClick = function(){};
            self.goToList = goToList;

            function onClickCadastrar(){

                if(Util.fn.isEmpty(self.model,['name','phone'])){
                    return MessageService.addError("Verifique os campos obrigatórios.");
                }

                Spinner.start();

                ApiService.save("/client/create",self.model)
                    .success(function(retorno){
                        Spinner.stop();
                        self.model = {};
                        MessageService.addSuccess("Novo cliente criado com sucesso");
                    }).error(function(err){
                    if(err){
                        Spinner.stop();
                        MessageService.addError(err.MESSAGE);
                    }
                });
            }

            function onClickChanges(){
                ApiService.update('/client/update',self.model).success(function(retorno) {
                    delete $rootScope.user;
                    MessageService.addSuccess("Cliente alterarado com sucesso!");
                    self.goToList('/listarcliente');

                }).error(function(data, status) {
                    MessageService.addError("Erro ao alterar cliente!");
                });
            }

            if(!Util.fn.isEmpty($rootScope,['cliente'])){
                var cliente = $rootScope.cliente;
                self.model._id = cliente._id;
                self.model.name = cliente.name;
                self.model.description = cliente.description;
                self.model.phone = cliente.phone;
                self.model.email = cliente.email;
                self.onClick = onClickChanges;
                self.isUpdate = true;
            }else{
                self.onClick  = onClickCadastrar;
                self.isUpdate = false;
            }

            function goToList(path) {
                $location.path(path);
            }

        }]);