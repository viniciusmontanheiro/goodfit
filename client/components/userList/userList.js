'use strict';

angular
    .module('app.userList', [])
    .controller('UserlistController', ['ApiService', 'MessageService','$rootScope','$location','Util', '$http','Spinner', UserlistController]);
    
function UserlistController(ApiService,MessageService,$rootScope,$location,Util, $http,Spinner) {

        var self = this;
        self.users = [];
        this.user = {};
        
        
        /**
         * Carregar lista de usuarios
         */
        init();
        function init(){
                Spinner.stop();
                Spinner.start();

                $http.get('user/list').then(function(retorno) {
                        Spinner.stop();
                        self.users = retorno.data.DATA;
                });
        }
               
        /**
        * onClickDelete 
        */
        this.onClickDelete = function(){
                if(!Util.fn.isEmpty(self.user,['_id'])){

                        Spinner.start();

                        ApiService.remove('/user/delete/',self.user._id).success(function(retorno) {
                                Spinner.stop();
                                MessageService.addSuccess("O usuário "+ self.user.name +" foi removido");
                                init();
                        }).error(function(data, status) {
                                Spinner.stop();
                                MessageService.addError("Erro ao remover o usuário " + self.user.name);
                        });
                }

        }

};