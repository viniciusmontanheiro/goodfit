'use strict';

angular.module('app.listar', [])
    .controller('ListarController', ['ApiService', 'MessageService','$rootScope','$location','Util','Spinner',
        function (ApiService,MessageService,$rootScope,$location,Util,Spinner) {

                var self = this;
                self.users = [];
                self.user = {};
                self.showGrid = false;

                self.onClickDelete = onClickDelete;
                self.goToChanges = goToChanges;
                self.init = init;
                self.getUser = getUser;
                self.init();

                function init(){

                        Spinner.stop();
                        Spinner.start();

                        ApiService.query('/user/list').success(function(retorno) {
                                Spinner.stop();
                                self.users = retorno.DATA;

                                if(self.users && self.users.length >= 1){
                                        self.showGrid = true;
                                }else{
                                        self.showGrid = false;
                                }

                        }).error(function(data, status) {
                                Spinner.stop();
                                self.showGrid = false;
                                MessageService.addError("Erro ao listar usuários")
                        });
                }

                function onClickDelete(){

                        if(!Util.fn.isEmpty(self.user,['_id'])){
                                Spinner.start();
                                ApiService.remove('/user/delete/',self.user._id).success(function(retorno) {
                                        Spinner.stop();
                                        MessageService.addSuccess("O usuário "+ self.user.name +" foi removido");
                                        self.init();
                                }).error(function(data, status) {
                                        Spinner.stop();
                                        MessageService.addError("Erro ao remover o usuário " + self.user.name);
                                });
                        }

                }

                function goToChanges(path,user) {
                        $rootScope.user = user;
                        $location.path(path);
                }

                function getUser(user) {
                        self.user = user;
                }

        }]);