/**
 * Created by vcrzy on 02/02/16.
 */
var mongoose = require('mongoose');

var historyShema = new mongoose.Schema({
    action : String,
    createDate : { type: Date, default: Date.now },
    entity: {type: Object},
    user : {type : Object}
});

module.exports = function(name){
    return mongoose.model(name , historyShema);
}